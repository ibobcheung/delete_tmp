# 删除临时文件和日志文件

该程序用于全盘扫描 .tmp 文件和超过 5M 的 .log 文件进行删除。并将执行结果记录在日志文件中。日志文件位于程序所在目录下的 logs 文件夹中。

## 分支说明

- master 分支: 脚本文件, 未使用GUI界面;
- gui 分支: 使用GUI界面, 待完善.

## 使用说明

### 1. 下载或克隆项目到本地

```bash
git clone https://github.com/your_username/delete_tmp.git
```

### 2. 创建虚拟环境并安装依赖

```bash
cd delete_tmp
python -m venv .venv

# Windows
.venv\Scripts\activate.bat

# Linux
source.venv/bin/activate

# 如果是gui 分支，需要安装依赖, master分支不需要安装依赖
pip install -r requirements.txt
```

### 3. 运行程序

```bash
python main.py
```

### 4. 打包程序

```bash
pyinstaller -i E://Workspace/project/delete_tmp/static/icon.ico -n 临时文件和日志文件清理 --version-file version.txt main.py
```

> 说明:
> 打包前先激活虚拟环境并安装依赖，在虚拟环境下进行打包。
> 在虚拟环境下安装PyInstaller。
> 项目目录下有version.txt文件，该文件用于指定程序的版本号。
> 项目目录下有static文件夹，该文件夹用于存放静态资源。
> 打包完成后，将项目中的db、static文件夹复制到exe所在目录，并确保和exe同级目录。
> 参数说明： 
> -w：表示不显示命令行窗口。 
> --add-data：表示添加数据文件。 
> -i：表示设置图标。 
> -n：表示设置程序名称。 
> --version-file：表示设置版本号。

![alt text](./static/image.png)
![alt text](./static/image2.png)

### 5. 查看日志

日志文件位于程序所在目录下的 logs 文件夹中。

## 注意事项

- 请确保在运行程序之前备份重要数据。
- 程序会删除所有符合条件的文件，请谨慎使用。
  