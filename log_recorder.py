# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/20 11:33
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: log_recorder.py
@IDE: PyCharm
@Python Version：
"""

import logging
import os
import time

"""
日志记录器: 封装Python的logging模块，实现日志记录功能，并提供日志文件按日期分割、日志文件过期自动清理功能。

功能：
1. 设置日志记录器
2. 日志文件按日期分割（当前日期加时间戳作为日志文件名）
3. 日志文件过期自动清理（清理30天前的日志文件）
4. 日志目录为./logs，日志文件名格式为 user_log_YYYYMMDD-HHMMSS.log
5. 日志级别为INFO，日志格式为 %(asctime)s - %(levelname)s - %(message)s
"""

log_path = "./logs"  # 日志目录


def cleanup_old_logs(log_directory=log_path, expiration_days=30):
    """
    清理过期的日志文件
    
    遍历指定的日志目录，删除超过指定天数未修改的日志文件
    
    参数:
    log_directory -- 日志文件所在的目录路径，默认为log_path
    expiration_days -- 日志文件的过期天数，默认为30天
    """
    now = time.time()  # 获取当前时间戳，自1970年1月1日以来的秒数
    cutoff_time = now - (expiration_days * 86400)  # 计算过期时间戳，86400为一天的秒数

    # 遍历日志目录中的每个文件
    for filename in os.listdir(log_directory):
        file_path = os.path.join(log_directory, filename)  # 拼接文件完整路径
        # 检查是否为文件且修改时间早于过期时间
        if os.path.isfile(file_path) and os.path.getmtime(file_path) < cutoff_time:
            os.remove(file_path)  # 删除过期文件
            # 记录删除操作的日志信息
            logging.debug(f'删除过期日志文件: {file_path}')


def setup_logger():
    """设置日志记录器"""
    log_directory = log_path  # 日志目录
    # 如果日志目录不存在，则创建日志目录
    os.makedirs(log_directory, exist_ok=True)  # 创建日志目录

    # 使用当前日期加时间戳作为日志文件名
    # current_time = time.strftime("%Y%m%d-%H%M%S")
    # log_file_name = f'user_log_{current_time}.log'

    # 使用当前日期作为日志文件名
    current_date = time.strftime("%Y%m%d")
    log_file_name = f'log_{current_date}.log'

    # 日志文件路径
    log_file_path = os.path.join(log_directory, log_file_name)

    # 设置日志基本配置
    logging.basicConfig(
        level=logging.INFO,  # 日志级别
        format='%(asctime)s - %(levelname)s - %(message)s',  # 日志格式
        handlers=[
            logging.FileHandler(log_file_path, mode='a', encoding='utf-8'),  # 写入日志文件
            logging.StreamHandler()  # 控制台输出
        ]
    )

    cleanup_old_logs(log_directory)  # 在设置完日志后清理过期日志
    return logging


if __name__ == '__main__':
    logger = setup_logger()
    logger.info('This is a test log message.')
